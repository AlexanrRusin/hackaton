﻿using System.Collections.Generic;
using UnityEngine;
using System;


[Serializable]
public class MaterialColorInfo
{
    public Material material;
    public Color color;
}

[Serializable]
public class SpaceColorInfo
{
    public Material material;
    public Color spaceColor;
    public Color starColor;
}

[Serializable]
public class LevelColorsInfo
{
    public List<MaterialColorInfo> materialsColors;
    public List<SpaceColorInfo> spaceColors;
}


public class GavnoLevelsColorsSettings : MonoBehaviour
{
    [SerializeField] List<LevelColorsInfo> levelsColorsInfos;

    int lastUsedLevelIndex = -1;


    public void RefreshColors(int levelIndex)
    {
        if (lastUsedLevelIndex != levelIndex)
        {
            while (levelIndex >= levelsColorsInfos.Count)
            {
                levelIndex -= levelsColorsInfos.Count;
            }

            for (int i = 0; i < levelsColorsInfos[levelIndex].materialsColors.Count; i++)
            {
                levelsColorsInfos[levelIndex].materialsColors[i].material.SetColor("_Color", levelsColorsInfos[levelIndex].materialsColors[i].color);
            }

            for (int i = 0; i < levelsColorsInfos[levelIndex].spaceColors.Count; i++)
            {
                levelsColorsInfos[levelIndex].spaceColors[i].material.SetColor("_SpaceColor", levelsColorsInfos[levelIndex].spaceColors[i].spaceColor);
                levelsColorsInfos[levelIndex].spaceColors[i].material.SetColor("_StarColor", levelsColorsInfos[levelIndex].spaceColors[i].starColor);
            }
        }

        lastUsedLevelIndex = levelIndex;
    }
}
