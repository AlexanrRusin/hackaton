﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(Stage))]
public class StageEditor : Editor
{
    Stage cachedStage;


    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (cachedStage == null)
        {
            cachedStage = target as Stage;
        }

        if (GUILayout.Button("Save rbl"))
        {
          Rigidbody[] foundedRb =  cachedStage.gameObject.GetComponentsInChildren<Rigidbody>();

            List<Rigidbody> rbToSave = new List<Rigidbody>();

            for(int i = 0; i < foundedRb.Length; i++)
            {
                if (!foundedRb[i].isKinematic)
                {
                    rbToSave.Add(foundedRb[i]);
                }
            }

            Reflection.SetValue(cachedStage, "dynamicRigidbodies", rbToSave);

            var prefabStage = PrefabStageUtility.GetCurrentPrefabStage();
            if (prefabStage != null)
            {
                EditorSceneManager.MarkSceneDirty(prefabStage.scene);
            }
        }
    }
}
