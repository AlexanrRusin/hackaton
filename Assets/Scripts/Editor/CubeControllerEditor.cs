﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(CubeController))]
public class CubeControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        bool areCubesShowing = PlayerPrefs.GetInt("areCubesShowing", 1) == 1;

        if (GUILayout.Button(areCubesShowing ? "Hide cubes" : "Show cubes"))
        {
            if (areCubesShowing)
            {
                DestroyCubesInRoot((target as CubeController).transform);
                PlayerPrefs.SetInt("areCubesShowing", 0);
            }
            else
            {
                PlayerPrefs.SetInt("areCubesShowing", 1);
                List<CubeSpawnInfo> cubesSpawnInfos = (List<CubeSpawnInfo>)(target as CubeController).GetType().GetField("cubesSpawnInfo", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).GetValue(target as CubeController);
                for (int i = 0; i < cubesSpawnInfos.Count; i++)
                {
                    Cube cube = Instantiate(AssetDatabase.LoadAssetAtPath<Cube>("Assets/Prefabs/Skins/Cube.prefab"), Vector3.zero, cubesSpawnInfos[i].rotation, (target as CubeController).transform);
                    cube.transform.localScale = cubesSpawnInfos[i].scale;
                    cube.transform.localPosition = cubesSpawnInfos[i].localPosition;
                    cube.IsPlayer = cubesSpawnInfos[i].isPlayer;
                    cube.Rigidbody.useGravity = cubesSpawnInfos[i].useGravity;
                }
            }
        }

        if (GUILayout.Button("Save Cubes"))
        {
            Cube[] cubesInRoot = (target as CubeController).GetComponentsInChildren<Cube>();

            if (cubesInRoot.Length == 0)
            {
                Debug.LogError("cant save 0 objects");
                return;
            }

            List<CubeSpawnInfo> cubesSpawnInfos = new List<CubeSpawnInfo>(cubesInRoot.Length);

            for (int i = 0; i < cubesInRoot.Length; i++)
            {
                cubesSpawnInfos.Add(new CubeSpawnInfo
                {
                    localPosition = cubesInRoot[i].transform.localPosition,
                    rotation = cubesInRoot[i].transform.rotation,
                    scale = cubesInRoot[i].transform.localScale,
                    isPlayer = cubesInRoot[i].IsPlayer,
                    useGravity = cubesInRoot[i].Rigidbody.useGravity
                });
            }

            (target as CubeController).GetType().GetField("cubesSpawnInfo", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).SetValue(target as CubeController, cubesSpawnInfos);

            DestroyCubesInRoot((target as CubeController).transform);

            var prefabStage = PrefabStageUtility.GetCurrentPrefabStage();
            if (prefabStage != null)
            {
                EditorSceneManager.MarkSceneDirty(prefabStage.scene);
            }
        }
    }


    void DestroyCubesInRoot(Transform root)
    {
        Cube[] cubesInRoot = root.GetComponentsInChildren<Cube>();

        for (int i = 0; i < cubesInRoot.Length; i++)
        {
            DestroyImmediate(cubesInRoot[i].gameObject);
        }
    }
}