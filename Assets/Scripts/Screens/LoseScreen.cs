﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoseScreen : Screen
{


    public override void CloseScreen(Action action)
    {
        base.CloseScreen(action);
    }

    public override void OpenScreen(Action action = null, bool closeWithDelayForAnimation = true)
    {
        base.OpenScreen(action, closeWithDelayForAnimation);
    }

    public void OnRestartClick()
    {
        PlayerPrefs.SetInt("NadoUsat", 1);
        Application.LoadLevel(Application.loadedLevel);
    }
}
