﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUI : Screen
{
    [SerializeField] StageBar stageBar;

    public override void CloseScreen(Action action = null)
    {
        base.CloseScreen(action);
    }

    public override void OpenScreen(Action action = null, bool closeWithDelayForAnimation = true)
    {
        base.OpenScreen(action, closeWithDelayForAnimation);
        stageBar.UpdateStagesBar(LevelManager.Instance.CurrentLevel.CurrentStageIndex,
                                   LevelManager.Instance.CurrentLevel.StagesPrefabs.Count,
                                    StatisticsManager.Instance.CurrentLevel+1);
    }

    void Awake()
    {
        Level.OnLevelStateChanged += Level_OnLevelStateChanged;
    }

    void OnDestroy()
    {
        Level.OnLevelStateChanged -= Level_OnLevelStateChanged;
    }

    public void OpenWinScreen()
    {
        ScreenManager.Instance.GetScreen<WinScreen>().OpenScreen();
    }
    public void OpenLoseScreen()
    {
        ScreenManager.Instance.GetScreen<LoseScreen>().OpenScreen();
    }

    void Level_OnLevelStateChanged(LevelState state)
    {
        switch (state)
        {
            case LevelState.WinStage:
            case LevelState.LoseStage:
                stageBar.UpdateStagesBar(LevelManager.Instance.CurrentLevel.CurrentStageIndex,
                                   LevelManager.Instance.CurrentLevel.StagesPrefabs.Count,
                                    StatisticsManager.Instance.CurrentLevel + 1);
                break;
        }
    }

    public void OnRestartButtonClick()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
}
