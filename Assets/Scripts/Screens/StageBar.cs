﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageBar : MonoBehaviour
{
    [SerializeField] Sprite completedStage, currentStage, nextStage;
    [SerializeField] RectTransform stagesParent;
    [SerializeField] float stageSize, lineWidth;
    [SerializeField] bool setRectsWithColor;
    [ConditionalHide("setRectsWithColor", true)][SerializeField] Color normalColor, nextColor;

    [SerializeField] Text currentLevelText, nextLevelText;
    [SerializeField] bool shouldDrawRects;
    [ConditionalHide("shouldDrawRects", true)] [SerializeField] Sprite completedRect, nextRect;

    List<Image> stagesImages;
    List<Image> imagesBetween;

    void SpawnStages(int count)
    {
        foreach (Transform child in stagesParent)
        {
            Destroy(child.gameObject);
        }
        float y = 0;
        float x = -stagesParent.sizeDelta.x / 2, rectX = x;
        float delta = stagesParent.sizeDelta.x / (count + 1);
        float distance = (stagesParent.sizeDelta.x - count * stageSize) / (count + 1);
        x += (distance + stageSize / 2);

        stagesImages = new List<Image>(count);
        imagesBetween = new List<Image>(count);

        for (int i = 0; i < count; i++)
        {
            Rect rect = UIHelper.CreateRect(new Vector2(x, y), new Vector2(stageSize, stageSize));
            stagesImages.Add(UIHelper.CreateImage(stagesParent, Color.white, completedStage, rect));
            x += distance + stageSize;
        }
        if (shouldDrawRects)
        {
            rectX += distance / 2;
            for (int i = 0; i < count + 1; i++)
            {
                Rect rect = UIHelper.CreateRect(new Vector2(rectX, y), new Vector2(distance, lineWidth));
                imagesBetween.Add(UIHelper.CreateImage(stagesParent, Color.white, completedRect, rect, false));
                rectX += distance + stageSize;
            }
        }
    }

    public void UpdateStagesBar(int currentStageIndex, int stagesCount, int currentLevel)
    {
        currentLevelText.text = currentLevel.ToString();
        nextLevelText.text = (currentLevel+1).ToString();
        if (stagesImages == null || stagesImages.Count != stagesCount)
        {
            SpawnStages(stagesCount);
        }

        for (int i = 0; i < stagesCount; i++)
        {
            if (i == currentStageIndex)
            {
                stagesImages[i].sprite = currentStage;
                if (setRectsWithColor)
                {
                    stagesImages[i].color = normalColor;
                }
            }
            else if (i > currentStageIndex)
            {
                stagesImages[i].sprite = nextStage;
                if (setRectsWithColor)
                {
                    stagesImages[i].color = nextColor;
                }
            }
            else if (i < currentStageIndex)
            {
                stagesImages[i].sprite = completedStage;
                if (setRectsWithColor)
                {
                    stagesImages[i].color = normalColor;
                }
            }
        }

        if (shouldDrawRects)
        {
            for (int i = 0; i < stagesCount + 1; i++)
            {
                if (i <= currentStageIndex)
                {
                    imagesBetween[i].sprite = completedRect;
                    if (setRectsWithColor)
                    {
                        imagesBetween[i].color = normalColor;
                    }
                }
                else
                {
                    imagesBetween[i].sprite = nextRect;
                    if (setRectsWithColor)
                    {
                        imagesBetween[i].color = nextColor;
                    }
                }
            }
        }
    }
}
