﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinScreen : Screen
{
    [SerializeField] Text levelText;


    public override void CloseScreen(Action action = null)
    {
        base.CloseScreen(action);
    }

    public override void OpenScreen(Action action = null, bool closeWithDelayForAnimation = true)
    {
        base.OpenScreen(action, closeWithDelayForAnimation);
        levelText.text = "LEVEL " + StatisticsManager.Instance.CurrentLevel;
    }

    public void OnContinueButtonClick()
    {
        LevelManager.Instance.StartLevel(StatisticsManager.Instance.CurrentLevel);
    }
}
