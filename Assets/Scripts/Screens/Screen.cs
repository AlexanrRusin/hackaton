﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Screen : MonoBehaviour
{
    [SerializeField] Animation anim;
    public bool isVisible;

    virtual public void OpenScreen(Action action = null, bool closeWithDelayForAnimation = true)
    {
        if(this != ScreenManager.Instance.currentScreen)
        {
            if (closeWithDelayForAnimation && ScreenManager.Instance.currentScreen != null)
            {
                ScreenManager.Instance.currentScreen.CloseScreen(() => { Open(action); });
            }
            else
            {
                if(ScreenManager.Instance.currentScreen != null)
                {
                    ScreenManager.Instance.currentScreen.CloseScreen();
                }
                Open(action);
            }

        }
    }

    void Open(Action action)
    {
        isVisible = true;
        gameObject.SetActive(isVisible);
        anim.Play(GetType().ToString());

        Scheduler.Instance.CallMethodWithDelay(() => {
            ScreenManager.Instance.currentScreen = this;
            action?.Invoke();
        }, anim.GetClip(GetType().ToString()).length);
    }

    virtual public void CloseScreen(Action action = null)
    {
        if (this == ScreenManager.Instance.currentScreen)
        {
            anim.Play(GetType()+"Close");
            Scheduler.Instance.CallMethodWithDelay(() => {
                action?.Invoke();
                isVisible = false;
                gameObject.SetActive(isVisible);
            }, anim.GetClip(GetType() + "Close").length);
        }
    }
}
