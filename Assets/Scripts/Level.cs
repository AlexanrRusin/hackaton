﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;


public enum LevelState
{
    None = 0,

    SpawnStage = 1,
    RunStage = 2,
    WinStage = 3,
    LoseStage = 4
}

public class Level : MonoBehaviour
{
    public static event Action<LevelState> OnLevelStateChanged;


    [SerializeField] List<Stage> stagesPrefabs;

    public List<Stage> SpawnedStages = new List<Stage>();
    
    LevelState currentLevelState;

    public List<Stage> StagesPrefabs => stagesPrefabs;

    public Stage CurrentStage { get; private set; }

    public int CurrentStageIndex { get; private set; }

    public LevelState CurrentLevelState
    {
        get => currentLevelState;
        set
        {
            if (CurrentLevelState != value)
            {
                currentLevelState = value;

                switch(currentLevelState)
                {
                    case LevelState.SpawnStage:
                        SpawnStage(CurrentStageIndex);
                        break;
                    case LevelState.WinStage:

                        CurrentStageIndex++;

                        if (CurrentStageIndex >= stagesPrefabs.Count)
                        {
                            StatisticsManager.Instance.CurrentLevel++;
                            ScreenManager.Instance.GetScreen<WinScreen>().OpenScreen();
                            EffectsManager.Instance.PlayEffect(EffectType.LevelWin, 2f, CameraController.Instance.confettiTransform.position, Color.white);
                        }
                        else
                        {
                            Scheduler.Instance.CallMethodWithDelay(() =>
                            {
                                CurrentLevelState = LevelState.SpawnStage;
                            }, 1f);
                        }

                        break;
                    case LevelState.LoseStage:

                        CameraController.Instance.ShakeCamera();
                        CurrentStage.CubeController.ExplodeFromObstacle();
                      
                        CameraController.Instance.MainCamera.GetComponent<GrayScale>().SetGrayScaleчатость(0f, 1f);
                        VibrationManager.Instance.PlayVibration(MoreMountains.NiceVibrations.HapticTypes.Failure);
                        Scheduler.Instance.CallMethodWithDelay(() =>
                        {
                            ScreenManager.Instance.GetScreen<LoseScreen>().OpenScreen();
                        }, 1f);
                        break;
                }

                OnLevelStateChanged?.Invoke(currentLevelState);
            }
        }
    }


    void Update()
    {
        switch(CurrentLevelState)
        {
            case LevelState.RunStage:
                CurrentStage.CubeController.UpdateMovement();
                break;
        }

        #if UNITY_EDITOR
        if (Input.GetMouseButtonDown(1))
        {
            for (int i = 0; i < CurrentStage.CubeController.cubes.Count; i++)
            {
                CurrentStage.CubeController.cubes[i].IsPlayer = true;
            }

            CurrentLevelState = LevelState.WinStage;
        }
        #endif
    }


    void SpawnStage(int index)
    {
        Vector3 stagePosition = Vector3.zero;

        Offset offset = null;

        if (index != 0)
        {
            offset = Instantiate(LevelManager.Instance.OffsetBetweenStagesPrefab, transform);
            offset.transform.position = CurrentStage.StageEndTransform.position - offset.BeginTransfrom.localPosition * offset.transform.localScale.z;

            stagePosition = offset.EndTransform.position - stagesPrefabs[index].StageBeginTransform.localPosition;
        }
        else
        {
            if (LevelManager.Instance.PreviousLevel != null)
            {
                offset = Instantiate(LevelManager.Instance.OffsetBetweenStagesPrefab, transform);
                offset.transform.position = LevelManager.Instance.PreviousLevel.SpawnedStages[LevelManager.Instance.PreviousLevel.SpawnedStages.Count - 1].StageEndTransform.position - offset.BeginTransfrom.localPosition * offset.transform.localScale.z;

                stagePosition = offset.EndTransform.position - stagesPrefabs[index].StageBeginTransform.localPosition;
            }
        }

        CurrentStage = Instantiate(stagesPrefabs[index], transform);
        SpawnedStages.Add(CurrentStage);

        if (index != 0)
        {
            Vector3 nextPlayerCubePos = Vector3.zero;

            for (int i = 0; i < CurrentStage.CubeController.CubesSpawnInfo.Count; i++)
            {
                if (CurrentStage.CubeController.CubesSpawnInfo[i].isPlayer)
                {
                    nextPlayerCubePos = CurrentStage.CubeController.CubesSpawnInfo[i].localPosition + stagePosition;

                }
            }

        //    for (int i = 0; i < SpawnedStages[index - 1].CubeController.cubes.Count; i++)
        //    {
        //        SpawnedStages[index - 1].CubeController.cubes[i].Rigidbody.isKinematic = true;
        //        SpawnedStages[index - 1].CubeController.cubes[i].GetComponent<BoxCollider>().isTrigger = true;
        //        GameObject qwe = new GameObject("Test");
        //        qwe.transform.parent = transform;
        //        qwe.transform.position = SpawnedStages[index - 1].CubeController.cubes[i].transform.position + Vector3.right * 3f.Random(2f);
        //        qwe.transform.position = new Vector3(0, qwe.transform.position.y, qwe.transform.position.z);
        //        SpawnedStages[index - 1].CubeController.cubes[i].transform.parent = qwe.transform;
        //        qwe.transform.DOMove(nextPlayerCubePos, 1f).SetEase(Ease.Linear);

        //        SpawnedStages[index - 1].CubeController.cubes[i].transform.DOLocalMove(Vector3.zero, 1f).OnComplete(() =>
        //        {
        //            qwe.gameObject.SetActive(false);
        //        });

        //        Vector3 initRot = Vector3.forward * 360f.Random();
        //        qwe.transform.Rotate(initRot, Space.World);
        //        qwe.transform.DORotate(Vector3.forward * 180 + initRot, 1).SetEase(Ease.Linear).SetLoops(-1, LoopType.Incremental);
        //    }
        }

        CurrentStage.transform.position = new Vector3(stagePosition.x, -20, stagePosition.z);

        for(int i = 0; i < CurrentStage.DynamicRigidbodies.Count; i++)
        {
            CurrentStage.DynamicRigidbodies[i].isKinematic = true;
        }

        MoveCameraToNewStage(CurrentStage.transform.position, () =>
        {
            CurrentStage.transform.DOMove(stagePosition, 1f).SetEase(Settings.Instance.platformUpCurve).OnComplete(()=>
            {
                for (int i = 0; i < CurrentStage.DynamicRigidbodies.Count; i++)
                {
                    CurrentStage.DynamicRigidbodies[i].isKinematic = false;
                }
            });

            CurrentStage.CubeController.SpawnAllCubes(()=>
            {
                CurrentLevelState = LevelState.RunStage;

                for (int i = 0; i < SpawnedStages.Count; i++)
                {
                    if (i != CurrentStageIndex)
                    {
                        SpawnedStages[i].gameObject.SetActive(false);
                    }
                }
            });
        });
    }


    void MoveCameraToNewStage(Vector3 position, Action callback)
    {
        CameraController.Instance.EmptyTransform.position = CameraController.Instance.TargetToFollow.position;
        CameraController.Instance.TargetToFollow = CameraController.Instance.EmptyTransform;

        CameraController.Instance.TargetToFollow.DOMove(position, 1f).OnComplete(() =>
        {
            callback?.Invoke();
        });
    }
}
