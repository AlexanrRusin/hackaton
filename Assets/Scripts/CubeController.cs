﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;
using System.Linq;

[Serializable]
public class CubeSpawnInfo
{
    public Vector3 localPosition;
    public Quaternion rotation;
    public Vector3 scale;
    public bool isPlayer;
    public bool useGravity;
}


public class CubeController : MonoBehaviour
{
    [SerializeField] List<CubeSpawnInfo> cubesSpawnInfo;
    [SerializeField] float oneCubeSpawnTime = 0.1f;
    public List<Cube> cubes = new List<Cube>();

    public List<CubeSpawnInfo> CubesSpawnInfo => cubesSpawnInfo;


    private void Awake()
    {
        Cube.OnCubeBecomePlayer += Cube_OnCubeBecomePlayer;
        Cube.OnGameOver += Cube_OnGameOver;
    }

    private void OnDestroy()
    {
        Cube.OnCubeBecomePlayer -= Cube_OnCubeBecomePlayer;
        Cube.OnGameOver -= Cube_OnGameOver;
    }

    public void SpawnAllCubes(Action callback)
    {
        Cube cubeToSpawn = SkinsManager.Instance.CurrentSkinCube;
        cubesSpawnInfo = cubesSpawnInfo.OrderBy(x => x.localPosition.z).ToList();
        Sequence sequence = DOTween.Sequence();

        for (int i = 0; i < cubesSpawnInfo.Count; i++)
        {
            Cube cube = Instantiate(cubeToSpawn, transform);
            cube.transform.localPosition = cubesSpawnInfo[i].localPosition + Vector3.up * 5 * (cubesSpawnInfo[i].isPlayer ? 0 : 1);
            cube.transform.rotation = cubesSpawnInfo[i].rotation;
            cube.transform.localScale = cubesSpawnInfo[i].scale;
            cube.IsPlayer = cubesSpawnInfo[i].isPlayer;
            cube.Rigidbody.useGravity = cubesSpawnInfo[i].useGravity;
            cubes.Add(cube);

            if (!cubesSpawnInfo[i].isPlayer)
            {
                cube.gameObject.SetActive(false);
                cube.Rigidbody.isKinematic = true;
            }

            if (i > 0 && Mathf.Approximately(cubesSpawnInfo[i - 1].localPosition.z, cubesSpawnInfo[i].localPosition.z))
            {
                sequence.Join(cube.transform.DOLocalMove(cubesSpawnInfo[i].localPosition, oneCubeSpawnTime).OnStart(()=>
                {
                    cube.gameObject.SetActive(true);
                }));
            }
            else
            {
                sequence.Append(cube.transform.DOLocalMove(cubesSpawnInfo[i].localPosition, oneCubeSpawnTime).OnStart(() =>
                {
                    cube.gameObject.SetActive(true);
                }));
            }
        }

        sequence.OnComplete(() =>
        {
            for (int i = 0; i < cubes.Count; i++)
            {
                cubes[i].Rigidbody.isKinematic = false;
            }

            callback?.Invoke();
        });
    }

    public void SetDifferentColors()
    {
        foreach(Cube cube in cubes)
        {
            Color color = cube.Renderer.material.color;
            cube.Renderer.material.color = color.SetRandomColor(Settings.Instance.ColorOffset);
        }
    }

    public void SetRainbowColors()
    {
        foreach (Cube cube in cubes)
        {
            if (cube != null && cube.IsPlayer)
            {
                Color color = Settings.Instance.RainbowColors.RandomObject();
                cube.Renderer.material.color = color;
            }
        }
    }

    Vector2 lastMousePosition;
    bool isFirstTouch = true;

    public void UpdateMovement()
    {
        if (Input.GetMouseButton(0))
        {
            Vector2 currentMousePosition = Input.mousePosition;
            if (isFirstTouch)
            {
                lastMousePosition = currentMousePosition;
                isFirstTouch = false;
            }

            for (int i = 0; i < cubes.Count; i++)
            {
                if (cubes[i] != null && cubes[i].IsPlayer)
                {
                    Vector3 delta = new Vector3(currentMousePosition.x - lastMousePosition.x, 0, currentMousePosition.y - lastMousePosition.y);
                    cubes[i].Rigidbody.AddForce(delta * Settings.Instance.Force);
                }
            }

            lastMousePosition = Input.mousePosition;
        }
        else
        {
            isFirstTouch = true;
        }
    }


    public void ExplodeFromObstacle()
    {
        for (int i = 0; i < cubes.Count; i++)
        {
            //cubes[i].GetComponent<BoxCollider>().isTrigger = true;
            Vector3 posToMove = -(gameOverObstaclePos - cubes[i].transform.position).normalized * 5000;
            //cubes[i].transform.DOMove(posToMove, 1f).SetEase(Settings.Instance.explodeCurve);
            posToMove.y = 3000;
            cubes[i].Rigidbody.mass = 10;
            cubes[i].Rigidbody.AddForce(posToMove);
        }
    }

    public void Remove(Cube cube)
    {
        cubes.Remove(cube);
    }

    void Cube_OnCubeBecomePlayer(Cube cube)
    {
        if (LevelManager.Instance.CurrentLevel.CurrentStage.CubeController == this && LevelManager.Instance.CurrentLevel.CurrentLevelState == LevelState.RunStage)
        {
            bool areAllCubesPlayers = true;
            for (int i = 0; i < cubes.Count; i++)
            {
                if (!cubes[i].IsPlayer)
                {
                    areAllCubesPlayers = false;
                    break;
                }
            }

            if (areAllCubesPlayers)
            {
                LevelManager.Instance.CurrentLevel.CurrentLevelState = LevelState.WinStage;
            }
        }
    }

    public Vector3 gameOverObstaclePos;
    void Cube_OnGameOver(Obstacle obstacle)
    {
        if (LevelManager.Instance.CurrentLevel.CurrentLevelState == LevelState.RunStage)
        {

        gameOverObstaclePos = obstacle.transform.position;
            //Debug.Log(LevelManager.Instance.CurrentLevel.CurrentStageIndex, LevelManager.Instance.CurrentLevel.CurrentStage);
            //Debug.Log("gameOverObstacle", gameOverObstaclePos.gameObject);
        LevelManager.Instance.CurrentLevel.CurrentLevelState = LevelState.LoseStage;
        }
    }
}
