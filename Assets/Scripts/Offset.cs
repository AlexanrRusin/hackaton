﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Offset : MonoBehaviour
{
    [SerializeField] Transform beginTransform;
    [SerializeField] Transform endTransform;


    public Transform BeginTransfrom => beginTransform;

    public Transform EndTransform => endTransform;
}
