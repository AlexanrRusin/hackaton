﻿using MoreMountains.NiceVibrations;
using UnityEngine;


[CreateAssetMenu(menuName = "ScriptableObjects/VibrationManager", fileName = "VibrationManager")]
public class VibrationManager : ScriptableSingleton<VibrationManager>
{
    [Header("IOS")]
    [SerializeField] bool useLimitOfCallsOnIOS;
    [SerializeField] [ConditionalHide("useLimitOfCallsOnIOS", true)] int numberOfCallsOnIOS;
    [SerializeField] [ConditionalHide("useLimitOfCallsOnIOS", true)] float callsDurationOnIOS;

    [Header("Android")]
    [SerializeField] bool useLimitOfCallsOnAndroid;
    [SerializeField] [ConditionalHide("useLimitOfCallsOnAndroid", true)] int numberOfCallsOnAndroid;
    [SerializeField] [ConditionalHide("useLimitOfCallsOnAndroid", true)] float callsDurationOnAndroid;

    int currentCall;
    bool isShedAvailable = true;


    public bool IsTurnedOn { get; set; } = true;

    
    bool IsVibrationAvailable
    {
        get
        {
            #if UNITY_EDITOR
            return false;
            #elif UNITY_IOS
            if (useLimitOfCallsOnIOS)
            {
                return currentCall < numberOfCallsOnIOS;
            }
            else
            {
                return true;
            }
            #elif UNITY_ANDROID
            if (useLimitOfCallsOnAndroid)
            {
                return currentCall < numberOfCallsOnAndroid;
            }
            else
            {
                return true;
            }
            #endif
        }
    }


    public void PlayVibration(HapticTypes hapticTypes, bool useDelay = false)
    {
        if ((!useDelay || IsVibrationAvailable) && IsTurnedOn)
        {
            MMVibrationManager.Haptic(hapticTypes);

            if (useDelay)
            {
                currentCall++;

                float callsDuration =
                #if UNITY_EDITOR
                0;
                #elif UNITY_IOS
                callsDurationOnIOS;
                #elif UNITY_ANDROID
                callsDurationOnAndroid;
                #endif

                isShedAvailable = false;
                Scheduler.Instance.CallMethodWithDelay(() =>
                {
                    currentCall = 0;
                    isShedAvailable = true;

                }, callsDuration);
            }
        }
    }
}
