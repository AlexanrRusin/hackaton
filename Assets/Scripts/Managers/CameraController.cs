﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class CameraController : Singleton<CameraController>
{
    [SerializeField] Camera mainCamera;
    [SerializeField] Vector3 offsetFromTarget;
    public Transform confettiTransform;

    Transform emptyTransform;


    public Camera MainCamera => mainCamera;

    public Transform TargetToFollow { get; set; }

    public Transform EmptyTransform
    {
        get
        {
            if (emptyTransform == null)
            {
                emptyTransform = (new GameObject("EmptyTransform")).transform;
            }

            return emptyTransform;
        }
        set
        {
            emptyTransform = value;
        }
    }


    void Update()
    {
        if (TargetToFollow != null && !isShake)
        {
            MainCamera.transform.position = new Vector3(0, offsetFromTarget.y, TargetToFollow.position.z + offsetFromTarget.z);
        }
    }

    bool isShake;
    public void ShakeCamera()
    {
        isShake = true;

        Vector3 initPos = MainCamera.transform.position;
        Sequence sequence = DOTween.Sequence();

        for(int i = 0; i < 5; i++)
        {
            Vector3 addedVector = new Vector3(1f.Random(-1f), 1f.Random(-1f), 1f.Random(-1f)) * 0.7f;
            sequence.Append(MainCamera.transform.DOMove(initPos + addedVector, 0.051f.Random(0.1f)));
        }

        sequence.Append(MainCamera.transform.DOMove(initPos, 0.1f.Random(0.3f)));

        sequence.OnComplete(() =>
        {
            isShake = false;
        });
    }
}

