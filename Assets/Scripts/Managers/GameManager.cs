﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    [SerializeField]
    List<GameObject> managers;

    protected override void Awake()
    {
        base.Awake();

        Application.targetFrameRate = 60;
        foreach(GameObject manager in managers)
        {
            Instantiate(manager, transform);
        }

        Scheduler.Instance.CallMethodWithDelay(StartGame, 0f);
    }


    void StartGame()
    {
        CameraController.Instance.TargetToFollow = transform;
        LevelManager.Instance.StartLevel(StatisticsManager.Instance.CurrentLevel);
    }
}
