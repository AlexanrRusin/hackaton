﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LevelManager : Singleton<LevelManager>
{
    [SerializeField] List<Level> levelsPrefabs;
    [SerializeField] Offset offsetBetweenStagesPrefab;
    [SerializeField] GavnoLevelsColorsSettings levelsColorsSettings;

    public Level PreviousLevel;


    public Level CurrentLevel { get; private set; }

    public Offset OffsetBetweenStagesPrefab => offsetBetweenStagesPrefab;


    protected override void Awake()
    {
        base.Awake();
        Level.OnLevelStateChanged += Level_OnLevelStateChanged;
    }


    private void OnDestroy()
    {
        Level.OnLevelStateChanged -= Level_OnLevelStateChanged;
    }


    public void StartLevel(int index)
    {
        PreviousLevel = CurrentLevel;

        if (index >= levelsPrefabs.Count)
        {
            Level levelToSpawn;

            if (PlayerPrefs.GetInt("NadoUsat") == 1)
            {
                levelToSpawn = levelsPrefabs[PlayerPrefs.GetInt("LevelIndex")];
            }
            else
            {
                levelToSpawn = levelsPrefabs.RandomObject();
            }
          
            CurrentLevel = Instantiate(levelToSpawn, transform);
            PlayerPrefs.SetInt("LevelIndex", levelsPrefabs.IndexOf(levelToSpawn));
        }
        else
        {
            CurrentLevel = Instantiate(levelsPrefabs[index], transform);
        }
        CurrentLevel.transform.position = Vector3.zero;
        CurrentLevel.CurrentLevelState = LevelState.SpawnStage;
        ScreenManager.Instance.GetScreen<GameUI>().OpenScreen();

        levelsColorsSettings.RefreshColors(index);
    }


    void Level_OnLevelStateChanged(LevelState levelState)
    {
        if (levelState == LevelState.RunStage && PreviousLevel != null)
        {
            Destroy(PreviousLevel.gameObject);
        }
    }
}
