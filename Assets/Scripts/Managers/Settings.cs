﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : Singleton<Settings>
{
    [SerializeField] float forceMuliplier;
    [SerializeField] float colorOffset;
    [SerializeField] float effectDuration;
    [SerializeField] float delay = 0.1f;
    [SerializeField] Color testColor;
    [SerializeField] Color[] rainbowColors;
    public float Force => forceMuliplier;
    public float ColorOffset => colorOffset;
    public float EffectDuration => effectDuration;
    public float Delay => delay;
    public Color TestColor => testColor;
    public Color[] RainbowColors => rainbowColors;
    public AnimationCurve platformUpCurve;
}
