﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public enum SkinType
{
    None        = 0,

    Default     = 1
}

[Serializable]
public class SkinInfo
{
    public SkinType skinType;
    public Cube cube;
}


public class SkinsManager : Singleton<SkinsManager>
{
    [SerializeField] List<SkinInfo> skins;


    public SkinType CurrentSkinType
    {
        get => (SkinType)PlayerPrefs.GetInt("CurrenSkinType", (int)SkinType.Default);
        set
        {
            PlayerPrefs.SetInt("CurrenSkinType", (int)value);
        }
    }


    public Cube CurrentSkinCube => skins.Find(x => x.skinType == CurrentSkinType).cube;
}
