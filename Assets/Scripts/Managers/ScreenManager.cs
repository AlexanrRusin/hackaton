﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenManager : Singleton<ScreenManager>
{
    [SerializeField] List<Screen> screensPrefabs;
    List<Screen> screens = new List<Screen>();
    public Screen currentScreen, previousScreen;

    protected override void Awake()
    {
        base.Awake();
        InitScreens();
        HideScreens();
    }

    void InitScreens()
    {
        foreach (Screen screen in screensPrefabs)
        {
            screens.Add(Instantiate(screen));
        }
    }

    void HideScreens()
    {
        foreach (Screen screen in screens)
        {
            screen.gameObject.SetActive(false);
        }
    }

    public void CloseCurrentScreen()
    {
        if (currentScreen != null)
        {
            currentScreen.CloseScreen();
        }
    }

    public T GetScreen<T>() where T : class
    {
        foreach (Screen screen in screens)
        {
            if (screen is T tmp)
            {
                return tmp;
            }
        }
        return null;
    }
}
