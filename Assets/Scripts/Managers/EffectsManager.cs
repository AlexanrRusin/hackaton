﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public enum EffectType
{
    None            = 0,

    DestroyCube     = 1,
    LevelWin        = 2
}


[Serializable]
public class EffectInfo
{
    public Effect effect;
    public EffectType effectType;
}


public class EffectsManager : Singleton<EffectsManager>
{
    [SerializeField] List<EffectInfo> effectsInfos;

    public Effect PlayEffect(EffectType effectType, float duration, Vector3 position, Color color, bool shouldDestroy = true)
    {
        EffectInfo effectInfo = effectsInfos.Find(x => x.effectType == effectType);
        Effect effect = Instantiate(effectInfo.effect, transform);
        effect.transform.position = position;
        if(effectType != EffectType.LevelWin)
        {
            effect.Renderer.material.color = color;
        }

        if (shouldDestroy)
        {
            Scheduler.Instance.CallMethodWithDelay(() =>
            {
                Destroy(effect);
            }, duration);
        }
        return effect;
    }
}
