﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatisticsManager : Singleton<StatisticsManager>
{
    public static event Action<int> OnLevelChanged;

    public int CurrentLevel
    {
        get
        {
            return PlayerPrefs.GetInt("CurrentLevel");
        }
        set
        {
            if(value != CurrentLevel)
            {
                PlayerPrefs.SetInt("CurrentLevel", value);
                OnLevelChanged?.Invoke(value);
            }
        }
    }
}
