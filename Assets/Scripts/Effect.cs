﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect : MonoBehaviour
{
    [SerializeField] Renderer renderer;
    [SerializeField] ParticleSystem particleSystem;
    public Renderer Renderer => renderer;
    public ParticleSystem ParticleSystem => particleSystem;
}
