﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class Cube : MonoBehaviour
{
    public static event Action<Cube> OnCubeBecomePlayer;
    public static event Action<Obstacle> OnGameOver;

    [SerializeField] Renderer renderer;
    [SerializeField] Rigidbody rigidbody;
    public Renderer Renderer => renderer;
    public Rigidbody Rigidbody => rigidbody;
    [SerializeField] bool isPlayer;
    public List<Cube> contacts = new List<Cube>();

    public bool IsPlayer
    {
        get
        {
            return isPlayer;
        }
        set
        {
            bool wasPlayer = isPlayer;
            isPlayer = value;
            if (value && !wasPlayer)
            {
                rigidbody.useGravity = true;
                Scheduler.Instance.CallMethodWithDelay(() => {
                    renderer.material.color = Settings.Instance.RainbowColors.RandomObject();
                    SetPlayersAround();
                    VibrationManager.Instance.PlayVibration(MoreMountains.NiceVibrations.HapticTypes.LightImpact);
                    OnCubeBecomePlayer?.Invoke(this);
                }, Settings.Instance.Delay);

            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Cube cube = collision.gameObject.GetComponent<Cube>();

        if (cube != null)
        {
            if (IsPlayer)
            {
                cube.IsPlayer = true;
            }
            else
            {
                contacts.Add(cube);
            }

            return;
        }

        if (IsPlayer)
        {
            Obstacle obstacle = collision.gameObject.GetComponent<Obstacle>();

            if (obstacle != null)
            {
                OnGameOver?.Invoke(obstacle);
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        Cube cube = collision.gameObject.GetComponent<Cube>();
        if (cube != null)
        {
            contacts.Remove(cube);
        }
    }

    void SetPlayersAround()
    {
        foreach(Cube cube in contacts)
        {
            if(cube != null)
            cube.IsPlayer = true;
        }
        contacts.Clear();
    }
}
