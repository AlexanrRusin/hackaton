﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrayScale : MonoBehaviour
{
    [SerializeField] Material material;


    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, material);
    }

    private void Awake()
    {
        material.SetFloat("_Multiplier", 0);
    }

    public void SetGrayScaleчатость(float initValue, float endValue)
    {
        StartCoroutine(Gavno(initValue, endValue));
    }


    IEnumerator Gavno(float initVal, float endVal)
    {
        float qwe = (endVal - initVal) / 60;

        for(int i = 0; i < 60; i++)
        {
            yield return null;
            material.SetFloat("_Multiplier", initVal + qwe * i);
        }
    }
}
