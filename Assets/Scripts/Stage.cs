﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage : MonoBehaviour
{
    [SerializeField] CubeController cubeController;
    [SerializeField] Transform stageBeginTransform;
    [SerializeField] Transform stageEndTransform;
    [SerializeField] List<Rigidbody> dynamicRigidbodies;

    public CubeController CubeController => cubeController;

    public Transform StageBeginTransform => stageBeginTransform;

    public Transform StageEndTransform => stageEndTransform;

    public List<Rigidbody> DynamicRigidbodies => dynamicRigidbodies;

}
