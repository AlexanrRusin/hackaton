﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObject : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        Cube cube = collision.gameObject.GetComponent<Cube>();
        if (cube != null)
        {
            EffectsManager.Instance.PlayEffect(EffectType.DestroyCube,
                                               Settings.Instance.EffectDuration,
                                               cube.transform.position,
                                               cube.Renderer.material.color);//Settings.Instance.TestColor.SetRandomColor(0.1f)
            LevelManager.Instance.CurrentLevel.CurrentStage.CubeController.Remove(cube);
            Destroy(cube.gameObject);
        }
    }
}
