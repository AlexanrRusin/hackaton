﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdsIntegrationService : MonoBehaviour
{
    private const string ironSourceIosApiKey = "9873897d"; //Change me つ ◕_◕ ༽つ
    private const string ironSourceAndroidApiKey = "9873c305"; //Change me つ ◕_◕ ༽つ
    
    [SerializeField] 
    Button _showRewardedButton;
    
    [SerializeField] 
    Button _showInterstitialButton;

    [SerializeField] 
    Text _isRewardedAvailable;
    
    [SerializeField] 
    Text _lastPlacement;
    
    [SerializeField] 
    Dropdown _bannerSize;
    
    [SerializeField] 
    Dropdown _bannerPosition;
    
    void Awake()
    {
        InitAds();
    }

    private void InitAds()
    {
#if UNITY_IOS
        IronSource.Agent.init (ironSourceIosApiKey, IronSourceAdUnits.REWARDED_VIDEO, IronSourceAdUnits.BANNER, IronSourceAdUnits.INTERSTITIAL);
#elif UNITY_ANDROID
        IronSource.Agent.init (ironSourceAndroidApiKey, IronSourceAdUnits.REWARDED_VIDEO, IronSourceAdUnits.BANNER, IronSourceAdUnits.INTERSTITIAL);
#endif
        IronSourceEvents.onRewardedVideoAvailabilityChangedEvent += RewardedVideoAvailabilityChangedEvent;
        IronSourceEvents.onInterstitialAdReadyEvent += InterstitialAdReadyEvent;
        IronSourceEvents.onInterstitialAdClosedEvent += InterstitialAdClosedEvent;
        IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;

        IronSource.Agent.validateIntegration();
        
        PopulateBannerSizes();
        IronSource.Agent.loadInterstitial();
    }

    private void InterstitialAdClosedEvent()
    {
        _showInterstitialButton.interactable = false;
        IronSource.Agent.loadInterstitial();
    }

    private void InterstitialAdReadyEvent()
    {
        _showInterstitialButton.interactable = true;
    }

    private void RewardedVideoAdRewardedEvent(IronSourcePlacement obj)
    {
        _lastPlacement.text = obj.ToString();
    }

    private void PopulateBannerSizes()
    {
        _bannerSize.options = new List<Dropdown.OptionData>
        {
            new Dropdown.OptionData(IronSourceBannerSize.BANNER.Description),
            new Dropdown.OptionData(IronSourceBannerSize.LARGE.Description),
            new Dropdown.OptionData(IronSourceBannerSize.SMART.Description),
            new Dropdown.OptionData(IronSourceBannerSize.RECTANGLE.Description)
        };
        
        _bannerPosition.options = new List<Dropdown.OptionData>
        {
            new Dropdown.OptionData(IronSourceBannerPosition.TOP.ToString()),
            new Dropdown.OptionData(IronSourceBannerPosition.BOTTOM.ToString())
        };
    }

    private void RewardedVideoAvailabilityChangedEvent(bool isAvailable)
    {
        _isRewardedAvailable.text = isAvailable.ToString();
        _showRewardedButton.interactable = isAvailable;
    }

    public void ShowRewarded()
    {
        IronSource.Agent.showRewardedVideo();
    }

    public void ShowInterstitial()
    {
        IronSource.Agent.showInterstitial();
    }

    public void LoadBanner()
    {
        Dictionary<string, IronSourceBannerSize> textToSize = new Dictionary<string, IronSourceBannerSize>()
        {
            {"BANNER", IronSourceBannerSize.BANNER},
            {"LARGE", IronSourceBannerSize.LARGE},
            {"SMART", IronSourceBannerSize.SMART},
            {"RECTANGLE", IronSourceBannerSize.RECTANGLE},
        };
        
        Dictionary<string, IronSourceBannerPosition> textToPosition = new Dictionary<string, IronSourceBannerPosition>()
        {
            {"TOP", IronSourceBannerPosition.TOP},
            {"BOTTOM", IronSourceBannerPosition.BOTTOM}
        };
        
        var size = textToSize[_bannerSize.options[_bannerSize.value].text];
        var position = textToPosition[_bannerPosition.options[_bannerPosition.value].text];
        IronSource.Agent.loadBanner(size, position);
    }

    public void ShowBanner()
    {
        IronSource.Agent.displayBanner();
    }
    public void HideBanner()
    {
        IronSource.Agent.hideBanner();
    }
    
    public void DestroyBanner()
    {
        IronSource.Agent.destroyBanner();
    }

    void OnApplicationPause(bool isPaused) 
    {                 
        IronSource.Agent.onApplicationPause(isPaused);
    }
}
