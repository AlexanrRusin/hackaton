﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


public class MenuItems : MonoBehaviour
{
    [MenuItem("Data/Reset All")]
    static void ResetAll()
    {
        PlayerPrefs.DeleteAll();
    }
}
#endif
