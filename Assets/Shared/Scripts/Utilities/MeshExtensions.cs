﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


#if UNITY_EDITOR
public static class MeshExtensions
{
    public static void SaveMesh(this Mesh mesh, string name, bool makeNewInstance, bool optimizeMesh)
    {
        string path = EditorUtility.SaveFilePanel("Save Separate Mesh Asset", "Assets/Models", name, "asset");

        if (string.IsNullOrEmpty(path))
        {
            return;
        }

        path = FileUtil.GetProjectRelativePath(path);
        Mesh meshToSave = (makeNewInstance) ? Object.Instantiate(mesh) as Mesh : mesh;

        if (optimizeMesh)
        {
            MeshUtility.Optimize(meshToSave);
        }

        AssetDatabase.CreateAsset(meshToSave, path);
        AssetDatabase.SaveAssets();
    }


    public static void SaveAssetInFolder<T>(this T obj, string directory, string name, bool makeNewInstance) where T : Object
    {
        string path = EditorUtility.SaveFilePanel("Save Asset", directory, name, "asset");

        if (string.IsNullOrEmpty(path))
        {
            return;
        }

        path = FileUtil.GetProjectRelativePath(path);
        T objToSave = (makeNewInstance) ? Object.Instantiate(obj) as T : obj;

        AssetDatabase.CreateAsset(objToSave, path);
        AssetDatabase.SaveAssets();
    }


    public static void SaveMaterial(this Material material, string name, bool makeNewInstance)
    {
        string path = EditorUtility.SaveFilePanel("Save Material Asset", "Assets/Materials", name, "asset");

        if (string.IsNullOrEmpty(path))
        {
            return;
        }

        path = FileUtil.GetProjectRelativePath(path);
        Material materialToSave = (makeNewInstance) ? Object.Instantiate(material) as Material : material;

        AssetDatabase.CreateAsset(materialToSave, path);
        AssetDatabase.SaveAssets();
    }
}
#endif 