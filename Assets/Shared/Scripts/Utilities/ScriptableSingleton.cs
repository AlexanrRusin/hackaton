﻿using UnityEngine;


public class ScriptableSingleton<T> : ScriptableObject where T : Object 
{
    #region Fields

    static T instance;

    #endregion



    #region Properties

    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                T[] assets = Resources.LoadAll<T>("");

                if (assets.Length > 0)
                {
                    instance = assets[0];
                    (instance as ScriptableSingleton<T>).Init();
                }
                else
                {
                    Debug.LogError("Cant find asset");
                }
            }

            return instance;
        }
    }

    #endregion



    #region Public Methods

    public void Initialize() { } //only to call Init

    #endregion



    #region Protected Methods

    protected virtual void Init() { }

    #endregion
}
