﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public static class Extensions
{
    public static void SetColor(this Button button, Color imageColor, Color textColor)
    {
        if(button.image != null)
        {
            button.image.color = imageColor;
        }
        foreach(Transform child in button.transform)
        {
            Graphic graphic = child.GetComponent<Graphic>();
            if (graphic != null)
            {
                Image image = (Image)graphic;
                Text text;
                if (image != null)
                {
                    image.color = imageColor;
                }
                else 
                {
                    text = (Text)graphic;
                    if (text != null)
                    {
                        text.color = textColor;
                    }
                }
            }
        }
    }

    public static void SetAlphaInAllChildren(this GameObject ga, float alpha)
    {
        Graphic graphic = ga.GetComponent<Graphic>();
        if (graphic != null)
        {
            graphic.color = graphic.color.SetAlpha(alpha);
        }
        foreach (Transform child in ga.transform)
        {
            Graphic graphicChild = child.GetComponent<Graphic>();
            Color resColor = graphicChild.color;
            graphicChild.color = resColor.SetAlpha(alpha);
        }
    }

    public static Color SetAlpha(this Color color, float alpha)
    {
        return new Color(color.r, color.g, color.b, alpha);
    }

    public static int Random(this int first,int second = 0)
    {
        if (first == second) return first;
        else if(first > second)
        {
            return UnityEngine.Random.Range(second, first);
        }
        else
        {
            return UnityEngine.Random.Range(first,second);
        }
    }

    public static float Random(this float first, float second = 0)
    {
        if (first > second)
        {
            return UnityEngine.Random.Range(second, first);
        }
        else
        {
            return UnityEngine.Random.Range(first, second);
        }
    }

    public static T RandomObject<T>(this IList<T> list)
    {
        return list[(list.Count - 1).Random()];
    }

    public static Color SetRandomColor(this Color color, float offset)
    {
        return new Color((color.r - offset).Random(color.r + offset),
                         (color.g - offset).Random(color.g + offset),
                         (color.b - offset).Random(color.b + offset),
                          color.a);
    }
}
