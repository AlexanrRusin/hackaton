﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHelper
{
    public static Text CreateText(RectTransform parent, string textString, Rect rect, Font font)
    {
        GameObject newText = new GameObject();
        Text text = newText.AddComponent<Text>();
        text.text = textString;
        RectTransform textRect = newText.GetComponent<RectTransform>();
        textRect.SetParent(parent);
        textRect.anchoredPosition = rect.position;
        textRect.sizeDelta = rect.size;
        text.alignment = TextAnchor.MiddleCenter;
        text.font = font;
        text.resizeTextForBestFit = true;
        text.resizeTextMaxSize = 100;
        textRect.localScale = new Vector3(1, 1, 1);
        return text;
    }

    public static Image CreateImage(RectTransform parent, Color color, Sprite sprite, Rect rect, bool preserveAspect = true)
    {
        GameObject newImage = new GameObject();
        Image image = newImage.AddComponent<Image>();
        image.sprite = sprite;
        RectTransform imageRect = newImage.GetComponent<RectTransform>();
        imageRect.SetParent(parent);
        imageRect.anchoredPosition = rect.position;
        imageRect.sizeDelta = rect.size;
        image.color = color;
        image.useSpriteMesh = true;
        imageRect.localScale = new Vector3(1, 1, 1);
        image.preserveAspect = preserveAspect;
        return image;
    }

    public static Rect CreateRect(Vector2 centerPosition, Vector2 size)
    {
        return new Rect
        {
            position = centerPosition,
            size = size
        };
    }
}
