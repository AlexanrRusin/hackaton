﻿public static class Reflection
{
    public static void SetValue(object obj, string field, object value)
    {
        obj.GetType().GetField(field, System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).SetValue(obj, value);
    }


    public static T GetValue<T>(object obj, string field)
    {
        return (T)obj.GetType().GetField(field, System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).GetValue(obj);
    }
}
