﻿Shader "Custom/Jelly/Simple-Normal"
{
    Properties
    {
        _DataTex ("Data texture", 2D) = "white" {}
        _GradientTex ("Gradient texture", 2D) = "white" {}
        [Normal] _NormalTex ("Normal", 2D) = "white" {}
        _HeightValue ("Height value", Range(0, 1)) = 0.2
        
        [Space(10)]
        _MinTransparency ("Min transparency", Range(0, 1)) = 0
        _MaxTransparency ("Max transparency", Range(0, 1)) = 1
        
        [Space(10)]
        _ShadowColor ("Shadow color", Color) = (1, 1, 1, 1)
        
        [Space(10)]
        _SpecColor ("Specular color", Color) = (1, 1, 1, 1)
        _InnerSpecColor ("Inner specular color", Color) = (1, 1, 1, 1)
        
        _SpecPower ("Specular power", float) = 32
        _InnerSpecPower ("Inner specular power", float) = 16
    }
    SubShader
    {
        Tags 
        { 
            "RenderType" = "Transparent" 
            "Queue" = "Transparent" 
            "IgnoreProjector" = "True"
        }

        Lod 100
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            Cull Front
            Tags { "LightMode" = "ForwardBase" }
        
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #define NORMAL_DIR -1
            
            #include "UnityCG.cginc"
            #include "BaseMath.cginc"
            #include "BasePass-Normal.cginc"
            ENDCG
        }
        Pass
        {
            Cull Back
            Tags { "LightMode" = "ForwardBase" }
        
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #define NORMAL_DIR 1
            
            #include "UnityCG.cginc"
            #include "BaseMath.cginc"
            #include "BasePass-Normal.cginc"
            ENDCG
        }
        Pass
        {
            Name "ShadowCaster"
            Tags { "LightMode" = "ShadowCaster" }
            
            ZWrite On
            ZTest LEqual
            Cull Back
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_shadowcaster
            
            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 bones : TEXCOORD1;
                float4 weights : TEXCOORD2;
            };

            struct v2f
            {
                float4 vec : SV_POSITION;
            };
            
            v2f vert (appdata v)
            {
                v2f o;
                
                float4 modelPos = float4(v.vertex.xyz, 1);
                float4 worldPos = mul(UNITY_MATRIX_M, modelPos);
                float3 normal = normalize(mul(UNITY_MATRIX_M, float4(v.normal, 0)));
                
                o.vec = mul(UNITY_MATRIX_VP, worldPos);
                
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return (length(i.vec) + unity_LightShadowBias.x) * _LightPositionRange.w;
            }
            ENDCG
        }
    }
}
