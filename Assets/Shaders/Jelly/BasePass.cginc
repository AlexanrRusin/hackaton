﻿struct appdata
{
    float4 vertex : POSITION;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float2 uv : TEXCOORD0;
    float4 bones : TEXCOORD1;
    float4 weights : TEXCOORD2;
};

struct v2f
{
    float4 vertex : SV_POSITION;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 worldPos : TEXCOORD0;
    float3 modelPos : TEXCOORD1;
    float3 objNormal : TEXCOORD2;
};

fixed4 _Color;

sampler2D _NoiseTex;
float4 _NoiseTex_ST;
float _ParallaxValue;

float _MinTransparency;
float _MaxTransparency;

fixed4 _SpecColor;
fixed4 _InnerSpecColor;
float _SpecPower;
float _InnerSpecPower;

float fresnel(float vdn)
{
    return max(0, vdn + 1);
}

float specular(float3 viewDir, float3 normal, float power)
{
    float3 reflection = reflect(_WorldSpaceLightPos0, normal);
    float vdr = dot(reflection, viewDir) * 0.5 + 0.5;
    
    return pow(vdr, power);
}

float diffuse(float3 normal)
{
    return dot(normal, _WorldSpaceLightPos0) * 0.5 + 0.5;
}

float2 triplanar_uv(float2 plane, float2 offset)
{
    float2 uv = plane * _NoiseTex_ST.xy + offset;
    return uv;
}

fixed4 noise_triplanar(float3 modelPos, float3 normal, float2 offset)
{
    fixed4 x = tex2D(_NoiseTex, triplanar_uv(modelPos.zy, offset));
    fixed4 y = tex2D(_NoiseTex, triplanar_uv(modelPos.xz, offset));
    fixed4 z = tex2D(_NoiseTex, triplanar_uv(modelPos.xy, offset));
    
    normal = saturate(pow(normal, 2));
    
    fixed4 col = z;
    col = lerp(col, x, normal.x);
    col = lerp(col, y, normal.y);
    
    return col;
}

float dencity(float3 modelPos, float3 viewDir, float3 tan, float3 normal, float3 worldNormal)
{   
    float3 bitan = cross(normal, tan);
    float3x3 tbn = transpose(float3x3(tan, bitan, normal));
    
    float3 tanView = mul(tbn, mul((float3x3)unity_WorldToObject, viewDir));
    
    return noise_triplanar(modelPos, normal, tanView.xy * _ParallaxValue);
}

v2f vert (appdata v)
{
    v2f o;
    
    float3 normal = normalize(mul(UNITY_MATRIX_M, float4(v.normal, 0)));
    
    float4 modelPos = float4(v.vertex.xyz, 1);
    float4 worldPos = mul(UNITY_MATRIX_M, modelPos);
    float3 newWorldPos = worldPos;
    
#ifdef DEFORMATION
    newWorldPos = apply_bones(worldPos.xyz, v.bones, v.weights);
    normal = apply_normal(modelPos.xyz, newWorldPos, worldPos.xyz, v.bones, v.weights, normal);
#endif
    
    worldPos.xyz = newWorldPos;
    
    o.modelPos = modelPos.xyz;
    o.worldPos = worldPos.xyz;
    o.vertex = mul(UNITY_MATRIX_VP, worldPos);
    o.normal = normal * NORMAL_DIR;
    o.tangent = normalize(v.tangent);
    o.objNormal = normalize(v.normal);
    
    return o;
}

fixed4 frag (v2f i) : SV_Target
{
    float3 viewDir = normalize(i.worldPos - _WorldSpaceCameraPos);
    float vdn = dot(viewDir, i.normal);

    fixed4 col = _Color;
    
    float spec = specular(viewDir, i.normal, _SpecPower);
    fixed4 specCol = _SpecColor;
    specCol.a = (col.a + specCol.a) * 0.5;
    col = lerp(col, specCol, spec);
     
    float d = dencity(i.modelPos, viewDir, i.tangent, i.objNormal, i.normal);
    
    col.a = fresnel(vdn);
    col.a += pow(d, 2);
    col.a = lerp(_MinTransparency, _MaxTransparency, min(1, col.a));
                
    //return float4(normal * 0.5 + 0.5, 1);
    return col;
}