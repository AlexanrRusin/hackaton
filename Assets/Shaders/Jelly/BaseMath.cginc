﻿uniform float3 _WorldBones[56];
uniform float3 _OrigLocalBones[56];

uniform float3 _PartPos;
float _MaxDist;
float _DisconnectDist;

float _WiggleValue;
float _WiggleFrequency;

float3 apply_height(float3 worldPos, float3 normal, float offset)
{
    return worldPos + normal * offset;
}

float3 get_bone_offset(float3 worldPos, int index, float weight)
{
    index -= 1;
    
    float3 origLocalBone = _OrigLocalBones[index];
    float3 origWorldBone = mul(UNITY_MATRIX_M, float4(origLocalBone, 1));
    
    float3 offset = _WorldBones[index] - origWorldBone;
    
    return index == -1 ? 0 : offset * weight;
}

float3 get_part_offset(float3 worldPos, float3 normal)
{
    float3 to = _PartPos - worldPos;
    float dist = length(to);
    float3 dir = normalize(to);
    
    float3 center = mul(UNITY_MATRIX_M, float4(0, 0, 0, 1));
    
    dir = normalize(worldPos - center);
    
    float t = max(0, _MaxDist - dist * 0.5) * _MaxDist * 1.3;
    float value = pow(t, 3);
    
    return value * dir;
}

float3 apply_bones(float3 worldPos, int4 bones, float4 weights)
{
    worldPos += get_bone_offset(worldPos, bones.x, weights.x);
    worldPos += get_bone_offset(worldPos, bones.y, weights.y);
    worldPos += get_bone_offset(worldPos, bones.z, weights.z);
    worldPos += get_bone_offset(worldPos, bones.w, weights.w);

    return worldPos;    
}

float3 apply_bones_and_parts(float3 worldPos, float3 normal, int4 bones, float4 weights)
{
    worldPos += get_bone_offset(worldPos, bones.x, weights.x);
    worldPos += get_bone_offset(worldPos, bones.y, weights.y);
    worldPos += get_bone_offset(worldPos, bones.z, weights.z);
    worldPos += get_bone_offset(worldPos, bones.w, weights.w);
    worldPos += get_part_offset(worldPos, normal);

    return worldPos;    
}

float3 apply_wiggle(float3 worldPos, float3 normal)
{
    float3 pos = worldPos * _WiggleFrequency + _Time.zzz;
    
    float sinx = sin(pos.x);
    float sinz = sin(pos.z);
    float3 dir = float3(sinx, 0, sinz);
    float value = ((sinx + sinz) * _WiggleValue) + _WiggleValue * 2;
    
    worldPos += normal * value;
    
    return worldPos;
}

float3 apply_normal(float3 localPos, float3 worldPos, float3 appliedWorldPos, int4 bones, float4 weights, float3 normal)
{
    float3 world_dx = worldPos + float3(2, 0, 0);
    float3 world_dz = worldPos + float3(0, 0, 2);
    
    float3 dx = apply_bones_and_parts(world_dx, normal, bones, weights) - appliedWorldPos;
    float3 dz = apply_bones_and_parts(world_dz, normal, bones, weights) - appliedWorldPos;
    
    dx = normalize(dx);
    dz = normalize(dz);
    
    float3 n = cross(dz, dx);
    float3x3 tbn = transpose(float3x3(dx, n, dz));
    
    //return normal;
    return normalize(mul(tbn, normal));
}