﻿struct appdata
{
    float4 vertex : POSITION;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float2 uv : TEXCOORD0;
    float4 bones : TEXCOORD1;
    float4 weights : TEXCOORD2;
};

struct v2f
{
    float4 vertex : SV_POSITION;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 bitan : TEXCOORD4;
    float3 worldPos : TEXCOORD0;
    float3 modelPos : TEXCOORD1;
    float albedo : TEXCOORD2;
    float2 uv : TEXCOORD3;
};

sampler2D _DataTex;
float4 _DataTex_ST;
float _HeightValue;

sampler2D _GradientTex;
sampler2D _NormalTex;

float _MinTransparency;
float _MaxTransparency;

fixed4 _ShadowColor;
fixed4 _SpecColor;
fixed4 _InnerSpecColor;
float _SpecPower;
float _InnerSpecPower;

void get_data(float2 uv, out fixed3 col, out float metallic, out float alpha, out float emission)
{
    fixed4 data = tex2D(_DataTex, uv);
    col = tex2D(_GradientTex, float2(data.x, 0.5));
    metallic = data.y;
    emission = data.z;
    alpha = data.w;
}

float fresnel(float vdn)
{
    return max(0, vdn + 1);
}

float specular(float3 viewDir, float3 normal, float power)
{
    float3 reflection = reflect(_WorldSpaceLightPos0, normal);
    float vdr = dot(reflection, viewDir) * 0.5 + 0.5;
    
    return pow(vdr, power);
}

float diffuse(float3 normal)
{
    return dot(normal, _WorldSpaceLightPos0) * 0.5 + 0.5;
}

float2 triplanar_uv(float2 plane, float2 offset)
{
    float2 uv = plane * _DataTex_ST.xy + offset;
    return uv;
}

v2f vert (appdata v)
{
    v2f o;
    
    o.uv = v.uv * _DataTex_ST.xy + _DataTex_ST.zw;
    
    float4 modelPos = float4(v.vertex.xyz, 1);
    float4 worldPos = mul(UNITY_MATRIX_M, modelPos);
    float3 newWorldPos = worldPos;

    float3 normal = normalize(mul(UNITY_MATRIX_M, float4(v.normal, 0)));
    float3 tangent = normalize(mul(UNITY_MATRIX_M, float4(v.tangent.xyz, 0)));
    float3 bitan = -cross(normal, tangent);
    
#ifdef DEFORMATION
    newWorldPos = apply_bones_and_parts(worldPos.xyz, normal, v.bones, v.weights);
    normal = apply_normal(modelPos.xyz, newWorldPos, worldPos.xyz, v.bones, v.weights, normal);
    worldPos.xyz = newWorldPos;
#endif

#ifdef WIGGLE
    newWorldPos = apply_wiggle(worldPos.xyz, normal);
    worldPos.xyz = newWorldPos;
#endif

//#ifdef NORMAL_DEFORMATION
    float height = tex2Dlod(_DataTex, float4(o.uv, 0, 0)).r * 2 - 1;
    worldPos.xyz = apply_height(worldPos, normal, height * _HeightValue);
//#endif
    
    o.modelPos = modelPos.xyz;
    o.worldPos = worldPos.xyz;
    o.vertex = mul(UNITY_MATRIX_VP, worldPos);
    
    o.normal = normal * NORMAL_DIR;
    o.tangent = tangent;
    o.bitan = bitan;

    o.albedo = dot(normal, _WorldSpaceLightPos0) * 0.5 + 0.5;
    
    return o;
}

fixed4 frag (v2f i) : SV_Target
{
    float3 viewDir = normalize(i.worldPos - _WorldSpaceCameraPos);
    float3x3 tbn = transpose(float3x3(i.tangent, i.bitan, i.normal));
    float3 nTex = UnpackNormal(tex2D(_NormalTex, i.uv));
    float3 normal = normalize(mul(tbn, nTex));
    
    float vdn = dot(viewDir, normal);

    fixed4 col = fixed4(0, 0, 0, 1);
    float metallic;
    float alpha;
    float emission;
    
    get_data(i.uv, col.rgb, metallic, alpha, emission);
    
    col.rgb = lerp(_ShadowColor, col.rgb, max(i.albedo, emission));
    
    float spec = specular(viewDir, normal, _SpecPower);
    fixed4 specCol = _SpecColor;
    specCol.a = (col.a + specCol.a) * 0.5;
    col = lerp(col, specCol, spec * metallic);
    
    col.a *= fresnel(vdn);
    col.a = lerp(_MinTransparency, _MaxTransparency, alpha * min(1, col.a));
    
    return col;
}