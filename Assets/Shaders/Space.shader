﻿Shader "Unlit/Space"
{
    Properties
    {
        _SpaceColor ("Space Color", Color) = (1,0,0,0)
        _StarColor ("Start Color", Color) = (1,0,0,0)
        _MainTex ("Texture", 2D) = "white" {}
        _NoiseTex ("Noise Tex", 2D) = "white" {}
        _Multiplier ("Multiplier", Float) = 0.8
        _Scale ("Scale", Float) = 0.22
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            
            sampler2D _NoiseTex;
            
            fixed4 _SpaceColor;
            fixed4 _StarColor;
            
            float _Multiplier;
            float _Scale;
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(fixed2(v.uv.x / _Scale, v.uv.y / _Scale), _MainTex);

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed c = tex2D(_NoiseTex, i.uv + _Time / 100).r;
                fixed4 col = tex2D(_MainTex, i.uv);
                
                if (col.r * c > _Multiplier)
                {
                    return _StarColor;
                }
                
                return _SpaceColor;
            }
            ENDCG
        }
    }
}
