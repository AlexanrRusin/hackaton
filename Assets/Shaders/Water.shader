﻿Shader "Custom/Water"
{
    Properties
    {
        _LightColor("Light Color", Color) = (0.486,0.921,1,1)
        _DarkColor("Dark Color", Color) = (0.466,0.713,1,1)
        _Speed("Speed", Float) = 0.27
        _ColorChangeOffset("ColorChangeOffset", Range(0,1)) = 0.21
    }
    SubShader
    {
        Tags { "RenderType"="Queue" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                float normal : NORMAL;
                float multiplier : COLOR0;
            };
            
            sampler2D _MainTex;
            float4 _MainTex_ST;
            
            fixed4 _LightColor;
            fixed4 _DarkColor;
            
            fixed4 _newLightColor;
            fixed4 _newDarkColor;
            
            float _zSize;
            
            float _Speed;
            fixed _ColorChangeOffset;
            
            float rand(float3 co)
            {
                return frac(sin(dot(co.xyz ,float3(12.9898,78.233,45.5432))) * 43758.5453);
            }

            v2f vert (appdata v, float3 normal : NORMAL)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                o.normal = normal;
                o.multiplier = saturate(v.vertex.z / _zSize);
                 
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {   
                fixed4 resCol;
                
                fixed4 _lightColor = lerp(_LightColor, _newLightColor, i.multiplier);
                fixed4 _darkColor = lerp(_DarkColor, _newDarkColor, i.multiplier);
                
                float t = abs(i.normal - (rand(i.normal) + _ColorChangeOffset) * abs((_Time.y * _Speed) % 2 - 1));
                resCol.rgb = lerp(_lightColor.rgb, _darkColor.rgb, t);
                
                return resCol;
            }
            ENDCG
        }
    }
}
